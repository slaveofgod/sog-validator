# bob-validator

[![NPM version][npm-image]][npm-url] [![Downloads][downloads-image]][npm-url] [![Build Status][travis-image]][travis-url]

A library of validators




[npm-url]: https://npmjs.org/package/bob-validator
[npm-image]: http://img.shields.io/npm/v/bob-validator.svg

[travis-url]: https://travis-ci.org/alexeybob/bob-validator
[travis-image]: http://img.shields.io/travis/alexeybob/bob-validator.svg

[coveralls-url]: https://coveralls.io/r/alexeybob/bob-validator
[coveralls-image]: http://img.shields.io/coveralls/alexeybob/bob-validator/master.svg

[downloads-image]: http://img.shields.io/npm/dm/bob-validator.svg