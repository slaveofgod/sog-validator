Object.assign(abv, function () {
    'use strict';

    /**
     * @constructor
     * @name abv.IdenticalToValidator
     * @extends abv.AbstractComparisonValidator
     * @classdesc
     * <p>Validates that a value is identical to another value, defined in the options.</p>
     * <p>This constraint compares using <code>===</code>, so <code>3</code> and "<code>3</code>" are not considered equal.</p>
     * @description
     * <p>Create a new Validator.</p>
     * @param {*} data The data which needs to be validated.
     * @param {Object} options The setting options
     * @param {Object} optionRules The validation rules for setting options.
     * @param {String} lang The language used by the application. Default: "<code>en</code>".
     * @param {Boolean} internal If this parameter is true, it means, that validation called from core.
     * @example
     * var validator = new abv.IdenticalToValidator(data, {"value": "the value to compare to"});
     * if (false === validator.isValid()) {
     *      validator.errors().first();
     * }
     */

    // PROPERTIES

    /**
     * @name abv.IdenticalToValidator#message
     * @type {String}
     * @description
     * <p>This is the message that will be shown if the value is not identical.</p>
     * <p>Default: "<code>This value should be identical to %%compared_value_type%% %%compared_value%%.</code>"</p>
     * <p>You can use the following parameters in this message:</p>
     * <table>
     *     <thead>
     *         <tr>
     *             <th>Parameter</th>
     *             <th>Description</th>
     *         </tr>
     *     </thead>
     *     <tbody>
     *         <tr>
     *             <td><code>%%compared_value%%</code></td>
     *             <td>The expected value</td>
     *         </tr>
     *         <tr>
     *             <td><code>%%compared_value_type%%</code></td>
     *             <td>The expected value type</td>
     *         </tr>
     *         <tr>
     *             <td><code>%%value%%</code></td>
     *             <td>The current (invalid) value</td>
     *         </tr>
     *     </tbody>
     * </table>
     */

    /**
     * @name abv.IdenticalToValidator#value
     * @type {*}
     * @description This option is required. It defines the value to compare to. It can be a string, number or object.
     */

    var IdenticalToValidator = function (data, options, optionRules, lang, internal) {
        abv.AbstractComparisonValidator.call(this, data, options, {
            message: optionRules.message || 'type:{"type":"string"}|length:{"min":3,"max":255}',
            value: optionRules.value || 'required'
        }, lang, internal);

        this.message = this.__options.message || 'This value should be identical to %%compared_value_type%% %%compared_value%%.';

        this.name = 'IdenticalToValidator';
    };
    IdenticalToValidator.prototype = Object.create(abv.AbstractComparisonValidator.prototype);
    IdenticalToValidator.prototype.constructor = IdenticalToValidator;

    Object.defineProperty(IdenticalToValidator.prototype, 'alias', {
        get: function () {
            return 'identical-to';
        }
    });

    Object.defineProperty(IdenticalToValidator.prototype, 'options', {
        get: function () {
            return [
                {
                    'name': 'value',
                    'type': 'any'
                }
            ];
        }
    });

    Object.assign(IdenticalToValidator.prototype, {
        /**
         * @private
         * @function
         * @name abv.IdenticalToValidator#__compareValues
         * @description Compare two value
         * @param {*} value Value
         * @param {*} comparedValue Compared value
         * @returns {Boolean}
         */
        __compareValues: function(value, comparedValue) {
            return (value === comparedValue);
        },

        /**
         * @private
         * @function
         * @name abv.IdenticalToValidator#__messageParameters
         * @description Returned parameters for error message which needs to be replaced
         * @returns {Object} List of parameters
         */
        __messageParameters: function () {
            return {
                'value': this.__formattedData(this.data),
                'compared_value': this.__formattedData(this.value),
                'compared_value_type': abv.getType(this.value)
            }
        }
    });

    return {
        IdenticalToValidator: IdenticalToValidator
    };
}());

abv.registry(abv.IdenticalToValidator);