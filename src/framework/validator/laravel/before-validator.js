Object.assign(abv, function () {
    'use strict';

    /**
     * @constructor
     * @name abv.BeforeValidator
     * @extends abv.AbstractValidator
     * @classdesc
     * <p>The field under validation must be a value preceding the given <code>date</code>.</p>
     * <p>The dates will be passed into the <code>Data object</code>.</p>
     * <p>In addition, like the after rule, the name of another field under validation may be supplied as the value of <code>date</code>.</p>
     * @description
     * <p>Create a new Validator.</p>
     * @param {*} data The data which needs to be validated.
     * @param {Object} options The setting options
     * @param {Object} optionRules The validation rules for setting options.
     * @param {String} lang The language used by the application. Default: "<code>en</code>".
     * @param {Boolean} internal If this parameter is true, it means, that validation called from core.
     * @example
     * var validator = new abv.BeforeValidator(data, {"value": "the value to compare to"});
     * if (false === validator.isValid()) {
     *      validator.errors().first();
     * }
     */

    // PROPERTIES

    /**
     * @name abv.BeforeValidator#value
     * @type {*}
     * @description
     * <p>This option is required.</p>
     * <p>It defines the value to compare to.</p>
     * <p>The data type could be <code>string</code>, <code>number</code> or <code>date</code>.<p>
     */

    var BeforeValidator = function (data, options, optionRules, lang, internal) {
        abv.LessThanValidator.call(this, data, {
            message: "The %%attribute%% must be a date before %%date%%.",
            value: options.value
        }, {
            value: 'required|type:{"type":["date","date-string"],"any":true}'
        }, lang, internal);

        this.name = 'BeforeValidator';
    };
    BeforeValidator.prototype = Object.create(abv.LessThanValidator.prototype);
    BeforeValidator.prototype.constructor = BeforeValidator;

    Object.defineProperty(BeforeValidator.prototype, 'alias', {
        get: function () {
            return 'before';
        }
    });

    Object.defineProperty(BeforeValidator.prototype, 'options', {
        get: function () {
            return [
                {
                    'name': 'value',
                    'type': 'date'
                }
            ];
        }
    });

    Object.assign(BeforeValidator.prototype, {
        /**
         * @private
         * @function
         * @name abv.BeforeValidator#__messageParameters
         * @description Returned parameters for error message which needs to be replaced
         * @returns {Object} List of parameters
         */
        __messageParameters: function () {
            return {
                'attribute': 'value',
                'date': this.data
            }
        }
    });

    return {
        BeforeValidator: BeforeValidator
    };
}());

abv.registry(abv.BeforeValidator);